import static org.junit.Assert.*;

import org.junit.Test;

public class PilaIntTest {
	
	@Test
	public void vaciaTest()
	{
		PilaInt pila=instancia();
		assertFalse(pila.vacia());
	}
	
	@Test
	public void agregarTest() 
	{
		PilaInt pila=instancia();
		pila.agregar(4);
		assertEquals((Integer)4, pila.cima());
	}
	
	@Test
	public void cimaTest()
	{
		PilaInt pilaVacia=new PilaInt();
		PilaInt pila=instancia();
		assertEquals((Integer)3, pila.cima());
		assertEquals((Integer)0, pilaVacia.cima());
	}
	@Test
	public void quitar()
	{
		PilaInt pila=instancia();
		pila.quitar();
		assertEquals((Integer)2, pila.cima());
	}
	
	private PilaInt instancia()
	{
		PilaInt pila= new PilaInt();
		pila.agregar(1);
		pila.agregar(2);
		pila.agregar(3);
		
		return pila;
	}

}
