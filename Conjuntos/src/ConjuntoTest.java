import static org.junit.Assert.*;

import org.junit.Test;

public class ConjuntoTest {

	@Test
	public void vacioTest() {
		Conjunto c=instancia();
		assertFalse(c.vacio());
	}
	
	@Test
	public void agregarTest()
	{
		Conjunto c=instancia();
		c.agregar(4);
		c.agregar(3);
		assertTrue(c.pertenece(4));
	}
	
	@Test
	public void eliminarTest()
	{
		Conjunto c=instancia();
		c.eliminar(3);
		c.eliminar(4);
		assertFalse(c.pertenece(3));
		assertFalse(c.pertenece(4));
	}
	
	@Test
	public void cantidadElementosTest()
	{
		Conjunto c=instancia();
		assertEquals(3, c.cantidadElementos());
	}
	
	@Test
	public void unionTest()
	{
		Conjunto c=instancia();
		Conjunto d=new Conjunto();
		d.agregar(5);
		d.agregar(1);
		c.union(d);
		assertTrue(c.pertenece(5));
	}
	
	@Test
	public void interseccion()
	{
		Conjunto c=instancia();
		Conjunto d=new Conjunto();
		d.agregar(1);
		d.agregar(2);
		d.agregar(4);
		c.interseccion(d);
		System.out.println(c.toString());
	}
	
	private Conjunto instancia()
	{
		Conjunto c=new Conjunto();
		c.agregar(1);
		c.agregar(2);
		c.agregar(3);
		
		return c;
	}
}
