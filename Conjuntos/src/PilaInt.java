import java.util.ArrayList;

public class PilaInt {
	private ArrayList<Integer> pila;

	public PilaInt(){
		pila= new ArrayList<Integer>();
	}			

	public boolean vacia()
	{
		return pila.isEmpty();
	}

	public void agregar (Integer i){
		pila.add(i);
	}

	public Integer quitar(){
		return pila.remove(pila.size()-1);
	}

	public Integer cima(){
		if(pila.size()!=0){
			return pila.get(pila.size()-1);
		}else{
			return (Integer)0;
		}
	}

	public String toString()
	{ 
		String datos="";
		PilaInt temp=new PilaInt();
		while (!this.vacia())
		{  datos+= this.cima()+",";
		temp.agregar(this.quitar());
		}
		while(!temp.vacia())
			this.agregar(temp.quitar());

		return datos;
	}

	public static void main(String [ ] args){
		PilaInt pila=new PilaInt();
		pila.agregar(1);
		pila.agregar(3);
		System.out.println(pila.toString());
	}

}

