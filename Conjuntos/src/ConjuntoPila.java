
public class ConjuntoPila {
	
	private PilaInt conjunto;
	
	public ConjuntoPila()
	{
		conjunto=new PilaInt();
	}
	
	public boolean vacio(){
		return conjunto.vacia();
	}
	
	public void agregar(Integer e)
	{
		if(!this.pertenece(e))
			this.conjunto.agregar(e);
	}
	
	public void eliminar(Integer e)
	{
		PilaInt aux=new PilaInt();
		while(!this.vacio()&&!conjunto.cima().equals(e))
			aux.agregar(conjunto.quitar());
		if(!vacio())
			conjunto.quitar();
		while(!aux.vacia())
			conjunto.agregar(aux.quitar());
	}
	
    public boolean pertenece(Integer e)
    {
    	boolean esta=false;
    	PilaInt aux=new PilaInt();
		while(!this.vacio()&&!conjunto.cima().equals(e))
			aux.agregar(conjunto.quitar());
		esta=!vacio();
		while(!aux.vacia())
			conjunto.agregar(aux.quitar());
		return esta;
    }
    
    public int cantidadElementos()
    {
    	int cantidad=0;
    	PilaInt aux=new PilaInt();
    	while(!this.vacio()){
			aux.agregar(conjunto.quitar());
    		cantidad++;
    	}
    	while(!aux.vacia())
    		conjunto.agregar(aux.quitar());
    	return cantidad;
    }
    
    @Override
    public String toString()
    {
    	return this.conjunto.toString();
    }
    
    public void union(ConjuntoPila c)
    {
    	while(!c.vacio())
    		{
    		if(!this.pertenece(c.elemento()))
    			conjunto.agregar(c.conjunto.quitar());
    		}
    }
    
    public void interseccion(ConjuntoPila c)
    {
    	while(!c.vacio())
    	{
    		if(!this.pertenece(c.elemento()))
    			this.conjunto.quitar();
    		c.conjunto.quitar();
    	}
    }
    
    public Integer elemento()
    {
    	return conjunto.cima();
    }
    
    public static void main(String [ ] args){
    	ConjuntoPila c = new ConjuntoPila();
//    	ConjuntoPila d = new ConjuntoPila();
    	c.agregar(1);
    	c.agregar(3);
//    	d.agregar(2);
//    	d.agregar(1);
		c.agregar(3);
//    	c.interseccion(d);
    	System.out.println(c.toString());
//    	System.out.println(d.toString());
    	
    }
}
