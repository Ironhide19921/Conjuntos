
public class PilaOrdenada<T extends Comparable<T>> extends PilaGenerica <T>{

	public PilaOrdenada() {
		super();
	}

	public void apilar(T elem){
		if(elem!=null){
			PilaGenerica<T> aux = new PilaGenerica<T>();
			super.apilar(elem);
			if(super.cantidadElementos()>1){
				while(!vacia()){
					aux.apilar(super.quitar());
				}
				while(!aux.vacia()){
					super.apilar(aux.minimo());
				}
			}
		}
	}
	public String toString(){
		return super.toString();
	}
	public T quitar(){
		return (T)super.quitar();
	}

	public static void main(String[] args) {
		PilaOrdenada<Integer> pila=new PilaOrdenada<Integer>();
		pila.apilar(3);
		pila.apilar(2);
		pila.apilar(1);
		PilaOrdenada<String>pila2=new PilaOrdenada<String>();
		pila2.apilar("bb");
		pila2.apilar("aa");
		pila2.apilar("cc");
		
		System.out.println(pila.toString());
		System.out.println("el minimo es: "+pila.minimo().toString());
		System.out.println(pila.toString());

		System.out.println(pila2.toString());

	}
}
