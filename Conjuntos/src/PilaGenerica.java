import java.util.ArrayList;

public class PilaGenerica <T extends Comparable<T>>{
	protected ArrayList<T> pila;

	protected PilaGenerica(){
		pila=new ArrayList<T>();

	}

	protected boolean vacia(){
		return pila.isEmpty();
	}

	protected void apilar(T elem){
		pila.add(elem);
	}

	public T minimo() {
		T min=null;
		if (!this.vacia()) {
			PilaGenerica<T> aux = new PilaGenerica<T>();
			min=quitar();
			while(cantidadElementos()>1){
				if(min.compareTo(cima())<0){
					aux.apilar(quitar());
				}
				else if(min.compareTo(cima())>0){
					aux.apilar(min);
					min=quitar();
				}
			}
			if(!vacia()){
				if(min.compareTo(cima())<0){
					aux.apilar(quitar());
				}
				else if(min.compareTo(cima())>0){
					aux.apilar(min);
					min=quitar();
				}
			}
			while(!aux.vacia()){
				apilar(aux.quitar());
			}
		}
		return min;
	}

	protected T quitar(){
		return pila.remove(pila.size()-1);
	}

	protected T cima(){
		if(pila.size()!=0){
			return pila.get(pila.size()-1);
		}
		else{
			return null;
		}
	}

	@Override
	public String toString(){
		String datos="";
		PilaGenerica<T> aux=new PilaGenerica<T>();
		while(!this.vacia()){
			aux.apilar(this.quitar());
		}
		while(!aux.vacia()){
			datos+=aux.cima()+",";
			this.apilar(aux.quitar());
		}
		return datos;
	}

	public int cantidadElementos(){
		return pila.size();
	}

	public static void main(String [ ] args){
		PilaGenerica<String> pila=new PilaGenerica<String>();
		pila.apilar("hola");
		pila.apilar("chau");
		System.out.println(pila.toString());
		PilaGenerica<Integer> pila1=new PilaGenerica<Integer>();
		pila1.apilar(10);
		pila1.apilar(20);
		System.out.println(pila1.toString());

	}




}
