
import static org.junit.Assert.*;

import org.junit.Test;

public class ConjuntoPilaTest {

	@Test
	public void vacioTest() 
	{
		ConjuntoPila c=instancia();
		assertFalse(c.vacio());
		ConjuntoPila d=new ConjuntoPila();
		assertTrue(d.vacio());
		
	}
	
	@Test
	public void agregarTest()
	{
		ConjuntoPila c=instancia();
		c.agregar(1);
		c.agregar(5);
		assertTrue(c.pertenece(5));
	}
	
	@Test
	public void eliminarTest()
	{
		ConjuntoPila c=instancia();
		c.eliminar(1);
		assertFalse(c.pertenece(1));
		ConjuntoPila d=new ConjuntoPila();
		d.eliminar(1);
	}
	
	@Test
	public void perteneceTest()
	{
		ConjuntoPila c=instancia();
		assertTrue(c.pertenece(1));
	}
	
	@Test
	public void cantidadElementosTest()
	{
		ConjuntoPila c=instancia();
		assertEquals(3, c.cantidadElementos());
	}
	
	@Test
	public void elementoTest()
	{
		ConjuntoPila c=instancia();
		assertEquals((Integer)1, c.elemento());
	}
	
	@Test
	public void unionTest()
	{
		ConjuntoPila c=instancia();
		ConjuntoPila d=new ConjuntoPila();
		d.agregar(4);
		d.agregar(1);
		d.agregar(5);
		
//		System.out.println(c.toString());
		c.union(d);
//		System.out.println(c.toString());
		assertTrue(c.pertenece(4));
		assertTrue(c.pertenece(5));
		
	}
	
	private ConjuntoPila instancia()
	{
		ConjuntoPila c = new ConjuntoPila();
		c.agregar(3);
		c.agregar(2);
		c.agregar(1);
		
		return c;
	}

}
