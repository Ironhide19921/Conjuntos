
import java.util.ArrayList;

public class Conjunto {
	
	
		ArrayList<Integer> datos;
		
		public Conjunto()
		{
			
			datos=new ArrayList<Integer>();
		}
		
		public boolean vacio()
		{
			return datos.isEmpty();
		}
		
		public void agregar(Integer e)
		{if (!this.pertenece(e))
				datos.add(e);			
		}
		
		
		public void eliminar(Integer e)
		{
			if (this.pertenece(e))
				datos.remove(datos.get(datos.indexOf(e)));
		}
		
		public boolean pertenece(Integer e)
		{   
		    return datos.contains(e);
		}
		
		
		public int cantidadElementos()
		{			
			return datos.size();
		}
		
		@Override
		public String toString()
		{  
			return datos.toString();				
		}
		
		public void union(Conjunto c)		
		{
			for (int i=0; i<c.cantidadElementos();i++)
			{
				if (!this.pertenece(c.elemento(i)))
					this.agregar(c.elemento(i));	
			}			
		}
		
		public void interseccion(Conjunto c)
		{
			for (int i=0; i<this.cantidadElementos();i++)
			{
				if (!c.pertenece(this.elemento(i)))
					this.eliminar(this.elemento(i));	
			}	
		}
		
		private Integer elemento(int i)
		{
			return datos.get(i);
		}	
}
