
public class Palabra <T>{
	
	private T objeto;
	
	public Palabra(T objeto){
		this.objeto=objeto;
	}
	
	public void setObjeto(T objeto){
		this.objeto=objeto;
	}
	
	public T getObjeto(){
		return objeto;
	}
	
	@Override
	public String toString() {
		return (String)this.getObjeto();
	}

	public void imprimir(){
		System.out.println("la palabra es "+this.getObjeto() );
	}

	public static void main(String[] args) {
		Palabra<Integer> p=new Palabra<Integer>(10);
//		System.out.println(p);
		p.imprimir();
		

	}

}
